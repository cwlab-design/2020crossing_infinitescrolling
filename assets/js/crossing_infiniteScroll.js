//生成新的容器裝第二篇
let second_art_area_new = document.createElement('div');
second_art_area_new.setAttribute('class','second_art_area');
second_art_area_new.setAttribute('data-num','1');
let article_box = document.querySelector(".article-box__main");
article_box.appendChild(second_art_area_new);


//0729 ie的進度條
var versions = function () {
  var u = navigator.userAgent, app = navigator.appVersion;
  var ua = navigator.userAgent.toLowerCase();
  return {
    trident: u.indexOf('Trident') > -1, //IE
  };
}();

// 進度條
$(window).on("scroll", function() {
  if (versions.trident) {
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("pageBar").style.width = scrolled + "%";
  }
  var scroll = $(window).scrollTop();
  var second_art_area = $(".second_art_area").offset().top;
  var second_art_area_height = $(".second_art_area").height();
  if (scroll < second_art_area - 100) {
    var winScroll = document.documentElement.scrollTop;
    var height = second_art_area - 72;
    var scrolled = (winScroll / height) * 100;
    document.getElementById("pageBar").style.width = scrolled + "%";
  } else {
    var winScroll2 = document.documentElement.scrollTop - second_art_area + 120;
    var height2 =
      $("body").height() - second_art_area - window.innerHeight + 120;
    var scrolled2 = (winScroll2 / height2) * 100;
    document.getElementById("pageBar").style.width = scrolled2 + "%";
  }
});

window.onload = function() {
  $(".article-box__side .side-content").css("transition", "none");
  $(window).on("scroll", function() {
    var article_video = $(".article-video").height();
    var scroll = $(window).scrollTop();
    var side_height = $(".side-content").height(),
      side_height_top = $(".article-box__side").offset().top;

    if (
      window.innerWidth > 768 &&
      scroll >
        $(".article-box").height() - window.innerHeight + article_video - 16
    ) {
      $(".side-content").css(
        "margin-top",
        $(".article-box").height() - $(".side-content").height() - 48
      );
      $(".side-content").css("position", "relative");
    } else {
      if (
        scroll > side_height + side_height_top - window.innerHeight - 32 &&
        window.innerWidth > 768
      ) {
        if (scroll > $(".article-box__side").height() - 32) {
          $(".side-content").css("position", "relative");
        } else {
          $(".side-content").css("position", "fixed");
          $(".side-content").css("bottom", "-32px");
        }
      } else {
        $(".side-content").css("position", "relative");
        $(".side-content").css("bottom", "0");
        $(".side-content").css("margin-top", "0");
      }
    }
  });
};

let art_num = 0,
  art_num_limited = 1;

if (art_num_limited == 1) {
  axios
    .get("./assets/js/test_data.json") //api網址
    .then(function(response) {
      const callback = (entries, observer) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            if (art_num <= art_num_limited) {
              //第二篇文章複製內容
              let article_box = document.querySelector(".article-box__main");
              let breadcrumb = document.querySelector(".breadcrumb");
              let article_titleBar = document.querySelector(
                ".article-titlebar"
              );
              let article_box_clone = article_box.cloneNode(true);
              let breadcrumb_clone = breadcrumb.cloneNode(true);
              let article_titleBar_clone = article_titleBar.cloneNode(true);
              let second_art_area = document.querySelector(".second_art_area");
              second_art_area.appendChild(breadcrumb_clone);
              second_art_area.appendChild(article_titleBar_clone);
              second_art_area.appendChild(article_box_clone);

              // art_title = response.data.msg;
              // $(".second_art_area .article-titlebar .title h1").html(art_title);

              //替換第二篇麵包屑
              $(".second_art_area .breadcrumb").html(
                '<div class="container"><nav aria-label="breadcrumb"><ul><li><a href="index.html">1</a></li><li><a href="channel.html">2</a></li><li><a href="channel-channelName.html">3</a></li></ul></nav></div>'
              );

              //替換第二篇 h1 response.data.msg
              $(".second_art_area .article-titlebar .title h1").html("測試測試這是第二篇的標題");
              //替換第二篇 response.data.msg
              $(".second_art_area .article-titlebar .info .info__author").html(
                '<a href="author.html">' + "vera/lab" + "</a>"
              );
              //替換第二篇日期 response.data.msg
              $(".second_art_area .article-titlebar .info .info__date").html(
                '<i class="icon-date"></i>' + "2020/10/11"
              );
              //替換第二篇觀看次數 response.data.msg
              $(".second_art_area .article-titlebar .info .info__view").html(
                '<i class="icon-view"></i>' + "12345"
              );

              $(".second_art_area .breadcrumb .container").removeClass(
                "container"
              );
              $(".second_art_area .article-titlebar .container").removeClass(
                "container"
              );

              //替換主圖
              $(".second_art_area .main-img__pic img").attr(
                "data-src",
                "assets/images/img/3.jpg"
              );

              //替換圖說（左）
              $(".second_art_area .main-img__info").html("vera");

              //替換圖說（右）
              $(".second_art_area .main-img__creadit").html(
                "／Photo Credit：Shutterstock"
              );

              //替換前言
              $(".second_art_area .main-introduction").html(
                "1992 年出生於炎熱的亞利桑那州，但討厭夏天，目前定居於台北。待過電影雜誌社、電影發行商，曾任金馬影展第 4 屆亞洲電影觀察團，過著電影即工作，工作即生活的日常，文章散見換日線、關鍵評論網、電影神搜、CATCHPLAY+ 等各大電影媒體平台。因麥可漢內克改變對影像世界的看法與想像，相信電影是每秒 24 格的謊言，也甘願一頭栽進謊言中。 1992 年出生於炎熱的亞利桑那州，但討厭夏天，目前定居於台北。"
              );

              //替換第二篇art內容
              $(".second_art_area .main-article-box article").html(
                response.data.items[0].content
              );

              //替換關鍵字
              $(".second_art_area .article-tag").html(
                '<ul><li><a href="search.html">11</a></li><li><a href="search.html">22</a></li><li><a href="search.html">33</a></li><li><a href="search.html">44</a></li><li><a href="search.html">55</a></li><li><a href="search.html">66</a></li><li><a href="search.html">77</a></li><li><a href="search.html">88</a></li></ul>'
              );

              //替換關聯閱讀 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
              $(".second_art_area .related .article-links__content").html(
                '<li><a href="">2【每月電影推薦】坎城、奧斯卡、「歐巴馬年度愛片」領銜，更有大量「非英語片」爭輝！</a></li><li><a href="">百年之後，《悲慘世界》的對立與衝突仍舊不斷上演</a></li>'
              );
              //替換作品推薦 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
              $(".second_art_area .recommend .article-links__content").html(
                '<li><a href="">3【每月電影推薦】坎城、奧斯卡、「歐巴馬年度愛片」領銜，更有大量「非英語片」爭輝！</a></li><li><a href="">百年之後，《悲慘世界》的對立與衝突仍舊不斷上演</a></li>'
              );
              //替換參考資料 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
              $(".second_art_area .reference .article-links__content").html(
                '<li><a href="">4【每月電影推薦】坎城、奧斯卡、「歐巴馬年度愛片」領銜，更有大量「非英語片」爭輝！</a></li><li><a href="">百年之後，《悲慘世界》的對立與衝突仍舊不斷上演</a></li>'
              );

              //替換作者 (不知道你們要怎麼拆，先整塊替換，如果需要細分再麻煩你們拆開來)
              $(".second_art_area .article-author").html(
                '<div class="article-author__img"><picture class="picture lr11"><div class="mask"></div><img data-src="assets/images/img/500x500-6.jpg" class="img lazy" alt="作者名稱"><noscript><img src="assets/images/img/500x500-6.jpg" class="img" alt="作者名稱"></noscript></picture><img src="assets/images/img/500x500-6.jpg" class="img ieonly" alt="作者名稱"> </div>	<div class="article-author__content"><div class="title">溫溫凱／地下電影 </div><div class="foreword">1992 年出生於炎熱的亞利桑那州，但討厭夏天，目前定居於台北。待過電影雜誌社、電影發行商，曾任金馬影展第 4 屆亞洲電影觀察團，過著電影即工作，工作即生活的日常，文章散見換日線、關鍵評論網、電影神搜、CATCHPLAY+ 等各大電影媒體平台。因麥可漢內克改變對影像世界的看法與想像，相信電影是每秒 24 格的謊言，也甘願一頭栽進謊言中。 </div><div class="btns"><div id="author-more__btn" class="btn btn--small btn--text">看更多<i class="icon-btn-arrow primary"></i></div></div></div>'
              );

              //goTop事件註冊
              $(".icon-goTop").click(function() {
                $("html, body").animate({ scrollTop: 0 }, 500);
                return false;
              });

              //刪除第二篇不需要的元素
              var self = document.querySelector(
                ".main-article .article-box__side"
              );
              if (self) {
                deletedNode = self.parentNode.removeChild(self);
              }

              var self2 = document.querySelector(
                ".main-article .article-shortcut"
              );
              if (self2) {
                deletedNode = self2.parentNode.removeChild(self2);
              }

              var self3 = document.querySelector(
                ".main-article .article-interest"
              );
              if (self3) {
                deletedNode = self3.parentNode.removeChild(self3);
              }
              var self4 = document.querySelector(
                ".main-article .article-video"
              );
              if (self4) {
                deletedNode = self4.parentNode.removeChild(self4);
              }
              var self5 = document.querySelector(".main-article .ads-box");
              if (self5) {
                deletedNode = self5.parentNode.removeChild(self5);
              }

              //lazyload
              let main_article_box_img = document.querySelectorAll(
                ".main-article-box .trackSection > img"
              );
              for (let i = 0; i < main_article_box_img.length; i++) {
                main_article_box_img_src = main_article_box_img[i].getAttribute(
                  "src"
                );
                main_article_box_img[i].setAttribute(
                  "data-src",
                  main_article_box_img_src
                );
                main_article_box_img[i].setAttribute(
                  "src",
                  "./assets/images/lazyload_bg.png"
                );
                $(".main-article-box .trackSection > img").addClass("lazyload");
              }
              $(".lazy").addClass("lazyload");
              $(".mask").css("z-index", "-1");

              lazyload();

              art_num++;
            }

            if (art_num == art_num_limited) {
              observer.unobserve(target);
            }
          }
        });
      };
      const observer = new IntersectionObserver(callback);
      const target = document.querySelector(".article-author");
      observer.observe(target);
    })
    .catch(function(error) {
      console.log(error);
    })
    .then(function() {});
}
